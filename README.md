# Confluence Attachment Migration Script

### Brian Johnson [Atlassian]

This script can be used to perform the procedure described in our [Restoring attachments from a filesystem backup to an imported space
](https://confluence.atlassian.com/confkb/restoring-attachments-from-a-filesystem-backup-to-an-imported-space-951392066.html) article when there are a large amount of attachments to be restored.